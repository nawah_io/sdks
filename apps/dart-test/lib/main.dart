import 'dart:developer' as developer;
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:nawah/main.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NawahWebsocket implements INawahWebsocket {
  @override
  create(NawahConfig config) async {
    return WebSocket.connect(config.api);
  }
}

class NawahLogger implements INawahLogger {
  @override
  log(Nawah nawah, NawahConfig config, dynamic values) {
    developer.log(values.toString());
  }

  @override
  info(Nawah nawah, NawahConfig config, dynamic values) {
    developer.log(values.toString(), level: Level.INFO.value);
  }

  @override
  warn(Nawah nawah, NawahConfig config, dynamic values) {
    developer.log(values.toString(), level: Level.WARNING.value);
  }

  @override
  error(Nawah nawah, NawahConfig config, dynamic values) {
    developer.log(values.toString(), level: Level.SEVERE.value);
  }
}

class NawahCache implements INawahCache {
  late SharedPreferences prefs;

  @override
  String? get(Nawah nawah, NawahConfig config, String key) {
    return prefs.getString(key);
  }

  @override
  void set(Nawah nawah, NawahConfig config, String key, String value) {
    prefs.setString(key, value);
  }

  @override
  void remove(Nawah nawah, NawahConfig config, String key) {
    prefs.remove(key);
  }

  @override
  void clear(Nawah nawah, NawahConfig config) {
    for (String key in prefs.getKeys()) {
      if (key.startsWith('nawah__${config.cacheKey}__')) {
        remove(nawah, config, key);
      }
    }
  }

  NawahCache() {
    SharedPreferences.getInstance().then((prefs) => this.prefs = prefs);
  }
}

void main() {
  runApp(const MyApp());

  nawahDI.set(NawahDIKey.websocket, NawahWebsocket());
  nawahDI.set(NawahDIKey.logger, NawahLogger());
  nawahDI.set(NawahDIKey.cache, NawahCache());
  Nawah nawah = Nawah();
  nawah.connect(
    api: 'ws://localhost:8081/ws',
    anonToken: '__ANON_TOKEN_033243444326853516113778',
    authAttrs: ['phone'],
    appId: 'KLIX_NS_IOS_2.0.1',
  );
  nawah
      .call(endpoint: 'user/read')
      .then(
        (res) => print(
          {'res': res}.toString(),
        ),
      )
      .catchError((err) => {
            print({'err': err}.toString())
          });
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
