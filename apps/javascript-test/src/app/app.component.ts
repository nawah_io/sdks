import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';
import { AUTH_STATE, Doc, CONN_STATE, NawahJS, Res } from '@nawah/javascript';
import { BehaviorSubject } from 'rxjs';

interface NawahLogEntry {
  datetime: string;
  res: Res<Doc>;
}
type NawahLog = Array<NawahLogEntry>;

@Component({
  selector: 'nawah-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit {
  apiUri: string =
    localStorage.getItem('nawah__api_uri') || 'ws://localhost/ws';
  anonToken: string =
    localStorage.getItem('nawah__anon_token') ||
    '__ANON_TOKEN_f00000000000000000000010';

  nawah = new NawahJS();
  authVar: string = localStorage.getItem('nawah__auth_var') || '';
  authVal: string = localStorage.getItem('nawah__auth_val') || '';
  password: string = localStorage.getItem('nawah__password') || '';

  callEndpoint = '';
  callSid = '';
  callToken = '';
  callQuery = '[]';
  callDoc = '{}';
  callCreateFileEndpoint = '';
  callDocFileAttr = '';
  @ViewChild('callDocFileInput', { read: ElementRef })
  callDocFileInput!: ElementRef;

  nawahLog$: BehaviorSubject<NawahLog> = new BehaviorSubject<NawahLog>([]);

  constructor() {
    this.nawah.addEventListener(
      'connStateChange',
      (connState) => connState == CONN_STATE.CONNCTED,
      () => {
        window.location.hash = 'tab-auth';
      }
    );

    this.nawah.addEventListener(
      'authStateChange',
      (authState) => authState == AUTH_STATE.AUTHED,
      () => {
        //if (!this.nawah.session) return;
        //this.callSid = this.nawah.session._id;
        //this.callToken = this.nawah.session.token;
      }
    );
  }

  ngOnInit(): void {
    window.location.hash = 'tab-init';
  }

  init(): void {
    localStorage.setItem('nawah__api_uri', this.apiUri);
    localStorage.setItem('nawah__anon_token', this.anonToken);

    this.callSid = 'f00000000000000000000012';
    this.callToken = this.anonToken;
    this.nawah = new NawahJS();

    this.nawah.addEventListener(
      'message',
      (res) => res?.args?.code != 'CORE_HEARTBEAT_OK',
      (_, res) => {
        this.nawahLog$.next([
          ...this.nawahLog$.value,
          {
            datetime: new Date().toISOString(),
            res,
          },
        ]);
      }
    );

    this.nawah.connect({
      api: this.apiUri,
      anonToken: this.anonToken,
      authAttrs: ['phone'],
      appId: 'TEST_APP',
      cacheKey: 'DEFAULT',
      debug: true,
    });
  }

  checkAuth(): void {
    this.nawah.checkAuth().catch((err) => {
      console.log('Error at `checkAuth`', { err });
    });
  }

  auth(): void {
    localStorage.setItem('nawah__auth_var', this.authVar);
    localStorage.setItem('nawah__auth_val', this.authVal);
    localStorage.setItem('nawah__password', this.password);
    this.nawah.auth(this.authVar, this.authVal, this.password).catch((err) => {
      console.log('Error at `auth`', { err });
    });
  }

  signout(): void {
    this.nawah.signout();
  }

  async call() {
    const doc = JSON.parse(this.callDoc);
    if (this.callDocFileAttr) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const callDocFileInput = this.callDocFileInput.nativeElement;
      eval(`doc.${this.callDocFileAttr} = callDocFileInput.files;`);
    }

    try {
      const res = await this.nawah.call({
        endpoint: this.callEndpoint,
        createFileEndpoint: this.callCreateFileEndpoint,
        query: JSON.parse(this.callQuery),
        doc: doc,
      });
      alert(`res: ${res.msg}`);
    } catch (err) {
      alert(`err: ${err?.msg || err}`);
    }
  }
}
