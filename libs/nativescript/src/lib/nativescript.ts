import {
  Doc,
  ICallArgs,
  IConfig,
  INawah,
  INawahCache,
  INawahFilesProcessor,
  INawahLogger,
  INawahWebsocket,
  Res,
  NawahBase,
  NawahDI,
} from '@nawah/javascript';
import { session } from '@nativescript/background-http';
import { ApplicationSettings, File } from '@nativescript/core';

class NativescriptWebsocket implements INawahWebsocket {
  create(config: IConfig) {
    require('nativescript-websockets');
    return new WebSocket(config.api);
  }
}

class NativescriptLogger implements INawahLogger {
  log(_: INawah, __: IConfig, ...values: Array<unknown>): void {
    console.log(`[LOG]`, ...values);
  }
  info(_: INawah, __: IConfig, ...values: Array<unknown>): void {
    console.log(`[INFO]`, ...values);
  }
  warn(_: INawah, __: IConfig, ...values: Array<unknown>): void {
    console.log(`[WARN]`, ...values);
  }
  error(_: INawah, __: IConfig, ...values: Array<unknown>): void {
    console.log(`[ERROR]`, ...values);
  }
}

class NativescriptCache implements INawahCache {
  get(_: INawah, config: IConfig, key: string): string | undefined {
    return (
      ApplicationSettings.getString(`nawah__${config.cacheKey}__${key}`) ||
      undefined
    );
  }
  set(_: INawah, config: IConfig, key: string, value: string): void {
    ApplicationSettings.setString(`nawah__${config.cacheKey}__${key}`, value);
  }
  remove(_: INawah, config: IConfig, key: string): void {
    ApplicationSettings.remove(`nawah__${config.cacheKey}__${key}`);
  }
  clear(_: INawah, config: IConfig): void {
    const keys = ApplicationSettings.getAllKeys().filter((key) =>
      key.startsWith(`nawah__${config.cacheKey}__`)
    );
    for (const key of keys) {
      ApplicationSettings.remove(key);
    }
  }
}
class NativescriptFilesProcessor implements INawahFilesProcessor {
  process(
    nawah: INawah,
    config: IConfig,
    callArgs: ICallArgs
  ): Array<Promise<Res<Doc>>> {
    if (callArgs.doc == undefined) {
      throw Error("No value for 'callArgs.doc'");
    }

    const files: { [key: string]: Array<File> } = {};
    const filesUploads: Array<Promise<Res<Doc>>> = [];
    for (const attr of Object.keys(callArgs.doc)) {
      // [DOC] Assert valid type before checking
      if (
        callArgs.doc[attr] instanceof Array &&
        (callArgs.doc[attr] as Array<unknown>).length &&
        (callArgs.doc[attr] as Array<unknown>)[0] instanceof File
      ) {
        NawahDI.get('logger').log(
          nawah,
          config,
          'log',
          'Detected File for doc attr: ',
          attr
        );
        files[attr] = callArgs.doc[attr] as Array<File>;
        callArgs.doc[attr] = [];

        NawahDI.get('logger').log(
          nawah,
          config,
          'log',
          'Attempting to read files from:',
          files[attr]
        );
        for (const i of Object.keys(files[attr])) {
          (callArgs.doc[attr] as Array<unknown>).push(
            files[attr][Number(i)].name
          );
          NawahDI.get('logger').log(
            nawah,
            config,
            'log',
            'Attempting to read file:',
            i,
            files[attr][Number(i)]
          );

          const url = `${config.api
            .replace('ws', 'http')
            .replace('/ws', '')}/file/create`;
          const description = 'ns-limp file upload';

          const request = {
            url: url,
            method: 'POST',
            headers: {
              'Content-Type': 'multipart/form-data',
              'X-Auth-Bearer': nawah.cacheSession?._id || '',
              'X-Auth-Token': nawah.cacheSession?.token || '',
              'X-Auth-App': config.appId,
            },
            description: description,
            androidAutoDeleteAfterUpload: false,
            androidNotificationTitle: 'Photo Uploading...',
          };
          const fileUpload: Promise<Res<Doc>> = new Promise<Res<Doc>>(
            (resolve, reject) => {
              const params = [
                {
                  name: '__module',
                  value: callArgs.endpoint.split('/')[0],
                },
                { name: '__attr', value: attr },
                { name: 'name', value: files[attr][Number(i)].name },
                {
                  name: 'type',
                  // eslint-disable-next-line @typescript-eslint/no-explicit-any
                  value: (files[attr][Number(i)] as any).type,
                },
                {
                  name: 'lastModified',
                  value: (Number(new Date().getTime()) / 1000)
                    .toFixed(0)
                    .toString(),
                },
                { name: 'file', filename: files[attr][Number(i)].path },
              ];
              const task = session(
                new Date().getTime().toString()
              ).multipartUpload(params, request);

              task.on('complete', () => {
                NawahDI.get('logger').log(
                  nawah,
                  config,
                  'log',
                  'File uploaded.... complete'
                );
              });

              task.on('responded', (e) => {
                NawahDI.get('logger').log(
                  nawah,
                  config,
                  'log',
                  'File uploaded....'
                );
                const res: Res<Doc> = JSON.parse(e.data);
                if (res.status != 200 || !res.args.count) {
                  reject(e);
                  return;
                }
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                (callArgs as any).doc[attr][i] = {
                  name: files[attr][Number(i)].name,
                  lastModified: files[attr][Number(i)].lastModified,
                  type: (files[attr][Number(i)] as never as { type: string })
                    .type,
                  size: files[attr][Number(i)].size,
                  ref: res.args.docs?.[0]._id,
                };
                resolve(res);
              });

              task.on('error', (e) => {
                NawahDI.get('logger').log(
                  nawah,
                  config,
                  'log',
                  'File upload error....',
                  e.responseCode,
                  e.response
                );
                reject(e);
              });
            }
          );
          filesUploads.push(fileUpload);
        }
      }
    }

    NawahDI.get('logger').log(
      nawah,
      config,
      'log',
      'Populated filesObservables:',
      filesUploads
    );

    return filesUploads;
  }
}

export class NawahNS extends NawahBase {
  constructor() {
    NawahDI.set('websocket', new NativescriptWebsocket());
    NawahDI.set('logger', new NativescriptLogger());
    NawahDI.set('cache', new NativescriptCache());
    NawahDI.set('filesProcessor', new NativescriptFilesProcessor());

    super();
  }
}
