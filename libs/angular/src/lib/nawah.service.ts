import { Injectable, Type } from '@angular/core';
import {
  AUTH_STATE,
  CallArgs,
  Doc,
  INIT_STATE,
  NawahBase,
  NawahJS,
  Res,
  SDKConfig,
  Session,
} from '@nawah/javascript';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class NawahService {
  __implementation: Type<NawahBase> = NawahJS;

  connections: Array<NawahBase> = [new this.__implementation()];
  #activeConnectionId = 0;

  get activeConnection(): NawahBase {
    return this.connections[this.#activeConnectionId];
  }

  get initState$(): Subject<INIT_STATE> {
    return this.activeConnection.initState$;
  }

  get authState$(): Subject<AUTH_STATE> {
    return this.activeConnection.authState$;
  }

  get session(): Session | undefined {
    return this.activeConnection.session;
  }

  setActiveConnection(id: number): void {
    if (id >= this.connections.length) {
      throw Error("Connection 'id' is invalid.");
    }

    this.#activeConnectionId = id;
  }

  init(config: SDKConfig, newConnection = false): Observable<Res<Doc>> {
    let connection: NawahBase;
    if (newConnection) {
      connection = new NawahBase();
      this.#activeConnectionId = this.connections.length;
      this.connections.push(connection);
    } else {
      connection = this.activeConnection;
    }

    return connection.init(config);
  }

  close(connection?: NawahBase): Observable<Res<Doc>> {
    connection ??= this.activeConnection;

    return connection.close();
  }

  reset(forceInited: boolean = false, connection?: NawahBase): void {
    connection ??= this.activeConnection;

    connection.reset(forceInited);
  }

  call<T extends Doc>(
    callArgs: CallArgs,
    connection?: NawahBase
  ): Observable<Res<T>> {
    connection ??= this.activeConnection;

    return connection.call(callArgs);
  }

  deleteWatch(
    watch: string | '__all',
    connection?: NawahBase
  ): Observable<Res<Doc>> {
    connection ??= this.activeConnection;

    return connection.deleteWatch(watch);
  }

  generateAuthHash(
    authVar: string,
    authVal: string,
    password: string,
    connection?: NawahBase
  ): string {
    connection ??= this.activeConnection;

    return connection.generateAuthHash(authVar, authVal, password);
  }

  auth(
    authVar: string,
    authVal: string,
    password: string,
    groups?: Array<string>,
    connection?: NawahBase
  ): Observable<Res<Doc>> {
    connection ??= this.activeConnection;

    return connection.auth(authVar, authVal, password, groups);
  }

  reauth(
    sid?: string,
    token?: string,
    groups?: Array<string>,
    connection?: NawahBase
  ): Observable<Res<Doc>> {
    connection ??= this.activeConnection;

    return connection.reauth(sid, token, groups);
  }

  signout(connection?: NawahBase): Observable<Res<Doc>> {
    connection ??= this.activeConnection;

    return connection.signout();
  }

  checkAuth(
    groups?: Array<string>,
    connection?: NawahBase
  ): Observable<Res<Doc>> {
    connection ??= this.activeConnection;

    return connection.checkAuth(groups);
  }
}
