/// An exception thrown by [NawahDI].set if called with key for already set
class KeyExistException {}

/// An exception thrown by [Nawah].connect if value of parameter `api` doesn't
/// correctly point to Nawah App, or due to any other connection issues
class ConnectionException {}

/// An exception thrown by [Nawah].signout if called when user is not authorised
class UserNotAuthorisedException {}

/// An exception thrown by [Nawah].callPipe if value of parameter `pipe` has
/// less than two calls
class InvalidPipeLengthException {}

/// An exception thrown by [Nawah].generateAuthHash uf value of parameter
/// `authVar` is not among defined `authAttrs` in [NawahConfig]
class InvalidAuthVarException {}

/// An exception thrown by [Nawah].generateAuthHash uf value of parameter
/// `password` doesn't match following criteria:
/// - Should be 8 characters at least.
/// - Contains, at least, 1 lower-case latin character.
/// - Containes, at least, 1 upper-case latin character.
/// - Contains, at least, 1 Arabic-numeral value.
class InvalidPasswordException {}

/// An exception thrown by [Nawah].auth if called when user already authorised
class UserAuthorisedException {}

/// An exception thrown by [Nawah].reauth if no values for `sid`, `token` are
/// cached, which are obtained from [NawahDI] set [INawahCache]
class NoCredsCachedException {}
