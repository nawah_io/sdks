enum ConnState {
  notConnected,
  connecting,
  connected,
  disconnected,
}

enum AuthState {
  notAuthed,
  authing,
  authed,
}

enum NawahQueueKey {
  auth,
  noAuth,
}

enum NawahListenerKey {
  message,
  connStateChange,
  authStateChange,
}

enum NawahQueryPart {
  pipe,
  special,
}

enum NawahQueryOper {
  $eq,
  $ne,
  $gt,
  $gte,
  $lt,
  $lte,
  $in,
  $nin,
  $all,
  $regex,
}

enum NawahQuerySpecialAttr {
  $skip,
  $limit,
  $attrs,
  $extn,
  $search,
}
