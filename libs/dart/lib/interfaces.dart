import 'nawah.dart';

typedef Res = Map<String, dynamic>;
typedef Doc = Map<String, dynamic>;

class NawahConfig {
  String api;
  String anonToken;
  List<String> authAttrs;
  String appId;
  bool debug;
  String cacheKey;

  NawahConfig({
    required this.api,
    required this.anonToken,
    required this.authAttrs,
    required this.appId,
    required this.debug,
    required this.cacheKey,
  });
}

abstract class INawahLogger {
  void log(Nawah nawah, dynamic values);
  void info(Nawah nawah, dynamic values);
  void warn(Nawah nawah, dynamic values);
  void error(Nawah nawah, dynamic values);
}

abstract class INawahCache {
  Future<String?> get(Nawah nawah, String key);
  Future<void> set(Nawah nawah, String key, String value);
  Future<void> remove(Nawah nawah, String key);
  Future<void> clear(Nawah nawah);
}

class NawahMessageListener {
  bool Function(Res res) filter;
  void Function(Nawah nawah, Res res) callable;
  bool once;
  String? tag;

  NawahMessageListener({
    required this.filter,
    required this.callable,
    this.once = false,
    this.tag,
  });
}

class NawahConnStateListener {
  bool Function(ConnState connState) filter;
  void Function(Nawah nawah, ConnState connState) callable;
  bool once;
  String? tag;

  NawahConnStateListener({
    required this.filter,
    required this.callable,
    this.once = false,
    this.tag,
  });
}

class NawahAuthStateListener {
  bool Function(AuthState authState) filter;
  void Function(Nawah nawah, AuthState authState) callable;
  bool once;
  String? tag;

  NawahAuthStateListener({
    required this.filter,
    required this.callable,
    this.once = false,
    this.tag,
  });
}

class NawahQuery {
  List<Map<String, dynamic>>? $pipe;
  Map<String, dynamic>? $special;

  NawahQuery({this.$pipe, this.$special}) {
    $pipe ??= [];
    $special ??= {};
  }

  Map<String, dynamic> toJson() {
    return {
      '\$pipe': $pipe,
      '\$special': $special,
    };
  }
}
