import 'dart:math';

import 'enums.dart';
import 'interfaces.dart';

String generateRandomString(int len) {
  var r = Random();
  return String.fromCharCodes(
    List.generate(len, (index) => r.nextInt(33) + 89),
  );
}

List<Doc> castListDynamicToListDoc(List<dynamic> listDynamic) {
  final listDoc = listDynamic.map((doc) => doc as Doc);
  return listDoc.toList();
}


Map<String, dynamic> nawahQueryPipeItem(
  String attr,
  NawahQueryOper oper,
  dynamic value,
) =>
    {
      attr: {oper.name: value}
    };

Map<String, List<Map<String, dynamic>>> nawahQueryPipeOr(
  List<Map<String, dynamic>> pipe,
) {
  return {
    '\$or': pipe,
  };
}

Map<String, dynamic> nawahQuerySpecial({
  int? $skip,
  int? $limit,
  List<String>? $attrs,
  List<String>? $extn,
  String? $search,
}) {
  Map<String, dynamic> querySpecial = {};
  if ($skip != null) {
    querySpecial['\$skip'] = $skip;
  }
  if ($limit != null) {
    querySpecial['\$limit'] = $limit;
  }
  if ($attrs != null) {
    querySpecial['\$attrs'] = $attrs;
  }
  if ($extn != null) {
    querySpecial['\$extn'] = $extn;
  }
  if ($search != null) {
    querySpecial['\$search'] = $search;
  }
  return querySpecial;
}
