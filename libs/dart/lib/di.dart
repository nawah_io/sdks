import 'exceptions.dart';

class NawahDI {
  static final Map<Type, Object?> _dependencies = {};

  static T? get<T>() {
    return _dependencies[T] as T?;
  }

  static void set(Type key, Object object) {
    if (_dependencies[key] != null) {
      throw KeyExistException();
    }

    _dependencies[key] = object;
  }
}
