library nawah;

import "dart:async";
import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:http/http.dart' as http;

import 'constants.dart';
import 'di.dart';
import 'enums.dart';
import 'exceptions.dart';
import 'interfaces.dart';
import 'utils.dart';

export 'constants.dart';
export 'di.dart';
export 'enums.dart';
export 'exceptions.dart';
export 'interfaces.dart';
export 'utils.dart';

class Nawah {
  NawahConfig? _config;

  NawahConfig? get config => _config;

  final Map<NawahQueueKey, List<Res>> _queue = {
    NawahQueueKey.auth: [],
    NawahQueueKey.noAuth: [],
  };

  final Map<NawahListenerKey, List<dynamic>> _listeners = {
    NawahListenerKey.message: [],
    NawahListenerKey.connStateChange: [],
    NawahListenerKey.authStateChange: [],
  };

  ConnState connState = ConnState.notConnected;
  AuthState authState = AuthState.notAuthed;

  Res? session;
  Res? cacheSession;

  Future<Map?> getCacheSession() async {
    if (cacheSession != null) {
      return Future.value(cacheSession);
    }

    final cacheSessionStr =
        await NawahDI.get<INawahCache>()?.get(this, 'session');

    if (cacheSessionStr != null) {
      cacheSession = jsonDecode(cacheSessionStr);
    } else {
      cacheSession = session;
    }

    return Future.value(cacheSession);
  }

  void setCacheSession(Map? session) {
    NawahDI.get<INawahCache>()?.set(this, 'session', jsonEncode(session));
    cacheSession = session as Res;
  }

  void clearCacheSession() {
    NawahDI.get<INawahCache>()?.remove(this, 'session');
    cacheSession = null;
  }

  void _onMessage(Res res) {
    // Loop over listeners
    _listeners[NawahListenerKey.message]!.forEachIndexed((i, listener) {
      listener = listener as NawahMessageListener;
      if (listener.tag != null) {
        NawahDI.get<INawahLogger>()?.log(
          this,
          'Attempting to run message Event listener with tag: ${listener.tag}',
        );
      }
      // Run filter, callable in try..catch to prevent errors arising from them breaking the loop
      try {
        // Check filter condition
        if (listener.filter(res)) {
          // Call listener callable
          listener.callable(this, res);
          // If once, delete listener
          if (listener.once) {
            _listeners[NawahListenerKey.message]!.removeAt(i);
          }
        }
      } catch (err) {
        NawahDI.get<INawahLogger>()?.error(
          this,
          'An error has occurred in running message Event listener: $err',
        );
      }
    });
  }

  void _onConnStateChange(ConnState connState) {
    // Prevent duplicate notifications
    if (this.connState == connState) return;
    // Update connState value
    this.connState = connState;
    // If new connState is ConnState.connected, attempt to clear noAuth queue
    if (connState == ConnState.connected) {
      for (Res call in _queue[NawahQueueKey.noAuth]!) {
        _sendCall(
          callArgs: call['callArgs'] as Res,
          completer: call['completer'] as Completer<Res>,
        );
      }
      _queue[NawahQueueKey.noAuth] = [];
    }
    // Loop over listeners
    _listeners[NawahListenerKey.connStateChange]!.forEachIndexed((i, listener) {
      listener = listener as NawahConnStateListener;
      if (listener.tag != null) {
        NawahDI.get<INawahLogger>()?.log(
          this,
          'Attempting to run connStateChange Event listener with tag: ${listener.tag}',
        );
      }
      // Run filter, callable in try..catch to prevent errors arising from them breaking the loop
      try {
        // Check filter condition
        if (listener.filter(connState)) {
          // Call listener callable
          listener.callable(this, connState);
          // If once, delete listener
          if (listener.once) {
            _listeners[NawahListenerKey.connStateChange]!.removeAt(i);
          }
        }
      } catch (err) {
        NawahDI.get<INawahLogger>()?.error(
          this,
          'An error has occurred in running connStateChange Event listener: $err',
        );
      }
    });
  }

  void _onAuthStateChange(AuthState authState) {
    // Prevent duplicate notifications
    if (this.authState == authState) return;
    // Update authState value
    this.authState = authState;
    // If new authState is AuthState.authed, attempt to clear auth queue
    if (authState == AuthState.authed) {
      for (Res call in _queue[NawahQueueKey.auth]!) {
        _sendCall(
          callArgs: call['callArgs'] as Res,
          completer: call['completer'] as Completer<Res>,
        );
      }
      _queue[NawahQueueKey.auth] = [];
    }
    // Loop over listeners
    _listeners[NawahListenerKey.authStateChange]!.forEachIndexed((i, listener) {
      listener = listener as NawahAuthStateListener;
      if (listener.tag != null) {
        NawahDI.get<INawahLogger>()?.log(
          this,
          'Attempting to run authStateChange Event listener with tag: ${listener.tag}',
        );
      }
      // Run filter, callable in try..catch to prevent errors arising from them breaking the loop
      try {
        // Check filter condition
        if (listener.filter(authState)) {
          // Call listener callable
          listener.callable(this, authState);
          // If once, delete listener
          if (listener.once) {
            _listeners[NawahListenerKey.authStateChange]!.removeAt(i);
          }
        }
      } catch (err) {
        NawahDI.get<INawahLogger>()?.error(
          this,
          'An error has occurred in running authStateChange Event listener: $err',
        );
      }
    });
  }

  Future<void> connect({
    required String api,
    required String anonToken,
    required List<String> authAttrs,
    required String appId,
    bool? debug,
    String? cacheKey,
  }) async {
    _config = NawahConfig(
      api: api,
      anonToken: anonToken,
      authAttrs: authAttrs,
      appId: appId,
      debug: debug ?? false,
      cacheKey: cacheKey ?? 'NAWAH',
    );

    // Confirm api points to Nawah application
    http.Response? response = null;
    try {
      response = await http.get(Uri.parse(api));
    } catch (err) {
      NawahDI.get<INawahLogger>()?.error(
        this,
        'Connecting to Nawah App failed with following condition: $err',
      );
      throw ConnectionException();
    }
    final isSuccessResponse = response.statusCode == 200;
    bool isNawahApp = true;
    try {
      final res = jsonDecode(response.body);
      isNawahApp = res['args']?['powered_by'] == 'Nawah';
    } on FormatException catch (_) {
      isNawahApp = false;
    } on TypeError catch (_) {
      isNawahApp = false;
    }

    if (!isSuccessResponse || !isNawahApp) {
      NawahDI.get<INawahLogger>()?.error(
        this,
        'Connecting to Nawah App failed with following condition:',
      );
      NawahDI.get<INawahLogger>()?.error(
        this,
        'isSuccessResponse: $isSuccessResponse',
      );
      NawahDI.get<INawahLogger>()?.error(
        this,
        'isNawahApp: $isNawahApp',
      );
      NawahDI.get<INawahLogger>()?.error(
        this,
        'Response statusCode, body: ${response.statusCode}, ${response.body}',
      );

      throw ConnectionException();
    }

    _onConnStateChange(ConnState.connected);
    this.addEventListener(
        NawahListenerKey.message, (res) => res['args']['session'] != null,
        (_, res) {
      NawahDI.get<INawahLogger>()?.log(this, 'Response has session object');

      if (res['args']['session']['_id'] == 'f00000000000000000000012') {
        if (this.authState == AuthState.authed) {
          session = null;
          this._onAuthStateChange(AuthState.notAuthed);
        }

        clearCacheSession();

        NawahDI.get<INawahLogger>()?.log(this, 'Session is null');
      } else {
        setCacheSession(res['args']['session']);
        session = res['args']['session'];
        _onAuthStateChange(AuthState.authed);
        NawahDI.get<INawahLogger>()?.log(this, 'Session updated');
      }
    });
  }

  Future<Res> call(
    String endpoint, {
    NawahQuery? query,
    Res? doc,
    bool awaitAuth = false,
  }) {
    Res callArgs = {
      'endpoint': endpoint,
      'query': query ?? NawahQuery(),
      'doc': doc ?? {},
      'awaitAuth': awaitAuth,
      'call_id': null,
    };

    final completer = Completer<Res>();

    // ignore: non_constant_identifier_names
    String call_id = generateRandomString(7);
    callArgs['call_id'] = call_id;
    _listeners[NawahListenerKey.message]!.add(
      NawahMessageListener(
        filter: (res) => res['args']['call_id'] == call_id,
        callable: (nawah, res) {
          NawahDI.get<INawahLogger>()?.log(
            this,
            'message received from observer on call_id: $call_id, $res',
          );
          if (res['status'] != 200) return completer.completeError(res);

          completer.complete(res);
        },
        once: true,
      ),
    );

    final isConnected = connState == ConnState.connected;
    final isAuthed = callArgs['awaitAuth'] && authState == AuthState.authed ||
        !callArgs['awaitAuth'];

    if (isConnected && isAuthed) {
      _sendCall(
        callArgs: callArgs,
        completer: completer,
      );

      return completer.future;
    }

    NawahDI.get<INawahLogger>()?.warn(
      this,
      'Connection not yet established, or require Authed status. Queuing call: $callArgs',
    );

    if (callArgs['awaitAuth']) {
      NawahDI.get<INawahLogger>()?.warn(this, 'Queuing in auth queue');
      _queue[NawahQueueKey.auth]!.add({
        'callArgs': callArgs,
        'completer': completer,
      });
    } else {
      NawahDI.get<INawahLogger>()?.warn(this, 'Queuing in noAuth queue');
      _queue[NawahQueueKey.noAuth]!.add({
        'callArgs': callArgs,
        'completer': completer,
      });
    }

    return completer.future;
  }

  Future<Res> callCreateFile(
    String endpoint, {
    required String attr,
    required String name,
    required http.ByteStream stream,
    required int size,
    required String type,
    required int lastModified,
  }) async {
    final completer = Completer<Res>();

    final uri = Uri.parse('${_config!.api}/$endpoint');

    final request = http.MultipartRequest('POST', uri);

    if (cacheSession?['_id'] != null) {
      request.headers['X-Auth-Bearer'] = cacheSession!['_id'];
    }
    if (cacheSession?['token'] != null) {
      request.headers['X-Auth-Token'] = cacheSession!['token'];
    }
    if (_config?.appId != null) {
      request.headers['X-Auth-App'] = _config!.appId;
    }

    request.fields['attr'] = attr;
    request.fields['lastModified'] = lastModified.toString();
    request.fields['type'] = type;
    request.fields['name'] = name;
    request.fields['size'] = size.toString();

    final multipartFile = http.MultipartFile(
      'content',
      stream,
      size,
      filename: name,
    );
    request.files.add(multipartFile);

    request.send().then((streamResponse) async {
      final response = await http.Response.fromStream(streamResponse);
      final res = _formatRes(response);

      if (res['status'] != 200) {
        completer.completeError(res);
        return;
      }

      res['args']['docs'][0]['file'] = {
        'name': name,
        'lastModified': lastModified,
        'type': type,
        'size': size,
        'ref': res['args']['docs'][0]['_id'],
      };

      completer.complete(res);
    });

    return completer.future;
  }

  Future<Res> callCreateFileDoc(
    String endpoint, {
    required String fileId,
    required String docId,
  }) {
    return call(
      endpoint,
      doc: {
        '_id': fileId,
        'doc': docId,
      },
    );
  }

  Future<List<Res>> callPipe(
    List<Future<Res> Function(List<Res>)> pipe,
  ) {
    // Make sure pipe has at least two calls, to simplify method structure
    if (pipe.length < 2) {
      throw InvalidPipeLengthException();
    }

    List<Res> resAggregate = [];
    Completer<List<Res>> completer = new Completer();

    Future(() async {
      int currentCall = -1;
      try {
        for (final call in pipe) {
          currentCall += 1;
          resAggregate.add(await call(resAggregate));
        }
        completer.complete(resAggregate);
      } catch (err) {
        NawahDI.get<INawahLogger>()?.error(
          this,
          'Error in call-pipe with call index $currentCall: $err',
        );
        try {
          // Wrap adding err to resAggregate with try..catch to handle non-Res value
          resAggregate.add(err as Res);
        } catch (_) {
          resAggregate.add({
            'status': 400,
            'msg': 'Unexpected error: $err',
            'args': {
              'code': 'UNEXPECTED_ERROR',
            }
          });
        }
        completer.completeError(resAggregate);
      }
    });

    return completer.future;
  }

  void _sendCall({
    required Res callArgs,
    required Completer<Res> completer,
  }) {
    String callArgsString = jsonEncode(callArgs);
    NawahDI.get<INawahLogger>()?.log(this, 'Sending call $callArgsString');

    final uri = Uri.parse('${_config!.api}/${callArgs['endpoint']}');

    final Doc postBody = {};

    if (callArgs['query'] != null) postBody['\$query'] = callArgs['query'];

    for (final MapEntry<dynamic, dynamic> entry in callArgs['doc'].entries) {
      postBody[entry.key] = entry.value;
    }

    http
        .post(uri,
            headers: {
              'Content-Type': 'application/json; charset=utf-8',
              if (cacheSession?['_id'] != null)
                'X-Auth-Bearer': cacheSession!['_id'],
              if (cacheSession?['token'] != null)
                'X-Auth-Token': cacheSession!['token'],
              if (_config?.appId != null) 'X-Auth-App': _config!.appId,
            },
            body: jsonEncode(postBody))
        .then((response) {
      NawahDI.get<INawahLogger>()?.log(
        this,
        'Call \'${callArgs['endpoint']}\' with \'call_id\' \'${callArgs['call_id']}\' response code: ${response.statusCode}',
      );
      final res = _formatRes(response);
      res['args']['call_id'] = callArgs['call_id'];
      _onMessage(res);
    }).catchError((err) {
      final res = {
        'status': 400,
        'msg': 'Unexpected error: $err',
        'args': {
          'code': 'UNEXPECTED_ERROR',
          'call_id': callArgs['call_id'],
        }
      };
      _onMessage(res);
    });
  }

  Res _formatRes(http.Response response) {
    try {
      return jsonDecode(response.body);
    } on FormatException catch (_) {
      NawahDI.get<INawahLogger>()?.error(
        this,
        'Failed to process value of Response as JSON: ${response.body}',
      );
      return {
        'status': response.statusCode,
        'msg': response.body,
        'args': {
          'code': response.reasonPhrase?.toUpperCase().replaceAll(' ', '_') ??
              'UNEXPECTED_ERROR',
        },
      };
    }
  }

  String generateAuthHash(String authVar, String authVal, String password) {
    if (!_config!.authAttrs.contains(authVar) && authVar != 'token') {
      throw InvalidAuthVarException();
    }

    if (passwordPattern.stringMatch(password) == null) {
      throw InvalidPasswordException();
    }

    return '$authVar$authVal$password${_config!.anonToken}';
  }

  Future<Res> auth(
    String authVar,
    String authVal,
    String password,
    List<String>? groups,
  ) {
    if (authState == AuthState.authed) {
      throw UserAuthorisedException();
    }

    authState = AuthState.authing;
    final Doc doc = {};

    // Wrap generateAuthHash with try..catch to revert authState if exception
    // is to be thrown
    try {
      doc['hash'] = generateAuthHash(authVar, authVal, password);
    } catch (err) {
      authState = AuthState.notAuthed;
      throw err;
    }

    doc[authVar] = authVal;
    doc['groups'] = groups ?? [];
    Future<Res> call = this
        .call(
      'session/auth',
      doc: doc,
    )
        .catchError((err) {
      _onAuthStateChange(AuthState.notAuthed);
      NawahDI.get<INawahLogger>()?.error(this, 'auth call err: $err');
    });

    return call;
  }

  Future<Res> reauth(
    String? sid,
    String? token,
    List<String>? groups,
  ) async {
    await getCacheSession();

    sid ??= cacheSession?['_id'];
    token ??= cacheSession?['token'];

    if (sid == null || token == null) {
      throw NoCredsCachedException();
    }
    _onAuthStateChange(AuthState.authing);
    NawahQuery query = NawahQuery($pipe: [
      nawahQueryPipeItem('_id', NawahQueryOper.$eq, sid),
      nawahQueryPipeItem('token', NawahQueryOper.$eq, token),
      nawahQueryPipeItem('groups', NawahQueryOper.$eq, groups ?? []),
    ]);
    Future<Res> call = this
        .call(
      'session/reauth',
      query: query,
    )
        .catchError((err) {
      NawahDI.get<INawahLogger>()?.error(this, 'reauth call err: $err');
      cacheSession = null;
      session = null;
      _onAuthStateChange(AuthState.notAuthed);
    });

    return call;
  }

  Future<Res> signout() {
    if (authState != AuthState.authed || session == null) {
      throw UserNotAuthorisedException();
    }

    Future<Res> call = this
        .call(
      'session/signout',
      query: NawahQuery($pipe: [
        nawahQueryPipeItem('_id', NawahQueryOper.$eq, session?['_id']),
      ]),
    )
        .catchError((err) {
      NawahDI.get<INawahLogger>()?.error(this, 'signout call err: $err');
    });

    return call;
  }

  Future<Res> checkAuth(List<String>? groups) {
    NawahDI.get<INawahLogger>()?.log(this, 'attempting checkAuth');

    Future<Res> call = reauth(null, null, groups);
    return call;
  }

  void addEventListener(NawahListenerKey event, filter, callable,
      {bool once = false, String? tag}) {
    Map<NawahListenerKey, dynamic> listenerObjects = {
      NawahListenerKey.message: NawahMessageListener(
        filter: filter,
        callable: callable,
        once: once,
        tag: tag,
      ),
      NawahListenerKey.connStateChange: NawahConnStateListener(
        filter: filter,
        callable: callable,
        once: once,
        tag: tag,
      ),
      NawahListenerKey.authStateChange: NawahAuthStateListener(
        filter: filter,
        callable: callable,
        once: once,
        tag: tag,
      ),
    };

    _listeners[event]!.add(listenerObjects[event]);
  }
}
