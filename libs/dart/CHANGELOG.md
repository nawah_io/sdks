## 2.0.9
Improve error handling

## 2.0.8
Refactor call-pipe for better error handling

## 2.0.7
Bugfix: Bad build

## 2.0.6
Introduce cast-list-dynamic-to-list-doc Utility
Improve call-pipe, refactor to only use list-res parameter

## 2.0.5
Bugfixes and improvements

## 2.0.4
Typing improvements

## 2.0.3
Introduce call-pipe to facilitate sequntial calls

## 2.0.2
Typing improvements

## 2.0.1
Drop Files Processor seqeunce in favour of call-create-file method

## 2.0.0
Initial release
