import { AUTH_STATE, CONN_STATE } from '../enums';
import { ICallArgs } from './call';
import { Doc, File, Res, Session } from './res';

export interface INawah {
  config: IConfig;
  connState: CONN_STATE;
  authState: AUTH_STATE;

  session: Session | undefined;
  cacheSession: Session | undefined;

  connect(config: IConfig): void;

  addEventListener<T extends Doc>(
    event: 'message',
    options: {
      condition: (res: Res<T>) => boolean;
      callable: (res: Res<T>) => void;
      once?: boolean;
      tag?: string;
    }
  ): void;
  addEventListener(
    event: 'connStateChange',
    options: {
      condition: (connState: CONN_STATE) => boolean;
      callable: (connState: CONN_STATE) => void;
      once?: boolean;
      tag?: string;
    }
  ): void;
  addEventListener(
    event: 'authStateChange',
    options: {
      condition: (authState: AUTH_STATE) => boolean;
      callable: (authState: AUTH_STATE) => void;
      once?: boolean;
      tag?: string;
    }
  ): void;

  call<T extends Doc>(endpoint: string, callArgs: ICallArgs): Promise<Res<T>>;

  callCreateFile(
    endpoint: string,
    attr: string,
    file: unknown,
  ): Promise<Res<File>>;

  generateAuthHash(authVar: string, authVal: string, password: string): string;

  auth(
    authVar: string,
    authVal: string,
    password: string,
    groups?: Array<string>
  ): Promise<Res<Doc>>;

  reauth(
    sid?: string,
    token?: string,
    groups?: Array<string>
  ): Promise<Res<Doc>>;

  signout(): Promise<Res<Doc>>;

  checkAuth(groups?: Array<string>): Promise<Res<Doc>>;
}

export interface IConfig {
  api: string;
  anonToken: string;
  authAttrs: Array<string>;
  appId: string;
  debug?: boolean;
  cacheKey?: string;
}

export interface INawahHttp {
  post<T>(
    nawah: INawah,
    endpoint: string,
    headers: { [key: string]: string },
    body: { [key: string]: unknown }
  ): Promise<Res<T>>;
}

export interface INawahLogger {
  log(nawah: INawah, ...values: Array<unknown>): void;
  info(nawah: INawah, ...values: Array<unknown>): void;
  warn(nawah: INawah, ...values: Array<unknown>): void;
  error(nawah: INawah, ...values: Array<unknown>): void;
}

export interface INawahCache {
  get(nawah: INawah, key: string): string | undefined;
  set(nawah: INawah, key: string, value: string): void;
  remove(nawah: INawah, key: string): void;
  clear(nawah: INawah): void;
}
