export interface NawahQueryPipe {
  [key: number]:
    | {
        [key: string]: {
          $eq: number | string | boolean | Array<number | string | boolean>;
        };
      }
    | {
        [key: string]: {
          $ne: number | string | boolean | Array<number | string | boolean>;
        };
      }
    | {
        [key: string]: {
          $regex: string;
        };
      }
    | {
        [key: string]: {
          $gt: number | string;
        };
      }
    | {
        [key: string]: {
          $gte: number | string;
        };
      }
    | {
        [key: string]: {
          $lt: number | string;
        };
      }
    | {
        [key: string]: {
          $lte: number | string;
        };
      }
    | {
        [key: string]: {
          $all: Array<number | string>;
        };
      }
    | {
        [key: string]: {
          $nin: Array<number | string>;
        };
      }
    | {
        [key: string]: {
          $in: Array<number | string>;
        };
      };
}

export interface NawahQuerySpecial {
  $search?: string;
  $sort?: {
    [attr: string]: 1 | -1;
  };
  $skip?: number;
  $limit?: number;
  $extn?: false | Array<string>;
  $attrs?: Array<string>;
  $group?: Array<{
    by: string;
    count: number;
  }>;
  $geo_near?: {
    val: [number, number];
    attr: string;
    dist: number;
  };
}

export interface NawahQuery {
  $pipe?: NawahQueryPipe;
  $special?: NawahQuerySpecial;
}
