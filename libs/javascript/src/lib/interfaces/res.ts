export interface Res<T> {
  args: ResArgs<T>;
  msg: string;
  status: number;
}

export interface ResArgs<T> {
  code?: string;
  watch?: string;
  docs?: T[];
  count?: number;
  total?: number;
  groups?: never;
  session?: Session;
  call_id: string;
}

export interface ResDocs<T> extends Res<T> {
  args: Pick<ResArgs<T>, 'docs' | 'count' | 'total' | 'groups' | 'call_id'>;
}

export interface ResCode<T> extends Res<T> {
  args: Pick<ResArgs<T>, 'code' | 'call_id'>;
}

export interface ResSession<T> extends Res<T> {
  args: Pick<ResArgs<T>, 'session' | 'call_id'>;
}

export interface Doc {
  _id: string;
  [key: string]: DocAttr;
}

export type DocAttr =
  | number
  | string
  | boolean
  | Doc
  | DocAttrDict
  | DocAttrArray
  | undefined;

export interface DocAttrDict {
  [key: string]: DocAttr | undefined;
}

export type DocAttrArray = Array<DocAttr | undefined>;

export interface Session extends Doc {
  user: User;
  host_add: string;
  user_agent: string;
  timestamp: string;
  expiry: string;
  token: string;
}

export interface User extends Doc {
  name: { [key: string]: string };
  locale: string;
  create_time: string;
  login_time: string;
  groups: Array<string>;
  privileges: { [key: string]: Array<string> };
  status: 'active' | 'banned' | 'deleted' | 'disabled_password';
}

export interface File extends Doc {
  user: string;
  doc: string;
  attr: string;
  file: {
    name: string;
    lastModified: number;
    type: string;
    size: number;
  };
  create_time: string;
}
