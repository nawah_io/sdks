import { NawahQuery } from './query';

export interface ICallArgs {
  call_id?: string;
  query?: NawahQuery;
  doc?: { [key: string]: unknown };
  awaitAuth?: boolean;
}
