import {
  INawah,
  INawahCache,
  INawahLogger,
  INawahHttp,
  Res,
  File,
} from './interfaces';
import { NawahBase } from './base';
import { NawahDI } from './di';

class JavascriptHttp implements INawahHttp {
  post<T>(
    nawah: INawah,
    endpoint: string,
    headers: { [key: string]: string },
    body: { [key: string]: unknown }
  ): Promise<Res<T>> {
    const callStr = JSON.stringify(body);
    NawahDI.get('logger').log(nawah, 'Sending call:', callStr);
    headers['Content-Type'] = 'application/json; charset=utf-8';
    return new Promise((resolve, reject) => {
      fetch(`${nawah.config.api}/${endpoint}`, {
        method: 'POST',
        headers: new Headers(headers),
        body: callStr,
      })
        .then((response) => {
          response.json().then((res) => resolve(res as Res<T>));
        })
        .catch((err) => {
          try {
            reject(JSON.parse(err));
          } catch (e) {
            reject(err);
          }
        });
    });
  }
}

class JavascriptLogger implements INawahLogger {
  log(_: INawah, ...values: Array<unknown>): void {
    console.log(...values);
  }
  info(_: INawah, ...values: Array<unknown>): void {
    console.log(...values);
  }
  warn(_: INawah, ...values: Array<unknown>): void {
    console.warn(...values);
  }
  error(_: INawah, ...values: Array<unknown>): void {
    console.error(...values);
  }
}

class JavascriptCache implements INawahCache {
  get(nawah: INawah, key: string): string | undefined {
    return (
      localStorage.getItem(`nawah__${nawah.config.cacheKey}__${key}`) ||
      undefined
    );
  }
  set(nawah: INawah, key: string, value: string): void {
    localStorage.setItem(`nawah__${nawah.config.cacheKey}__${key}`, value);
  }
  remove(nawah: INawah, key: string): void {
    localStorage.removeItem(`nawah__${nawah.config.cacheKey}__${key}`);
  }
  clear(nawah: INawah): void {
    for (let i = 0; i < localStorage.length; i++) {
      const key = localStorage.key(i);
      if (!key) continue;
      if (key.startsWith(`nawah__${nawah.config.cacheKey}__`)) {
        this.remove(nawah, key);
      }
    }
  }
}

export class NawahJS extends NawahBase {
  constructor() {
    NawahDI.set('http', new JavascriptHttp());
    NawahDI.set('logger', new JavascriptLogger());
    NawahDI.set('cache', new JavascriptCache());

    super();
  }

  callCreateFile(
    endpoint: string,
    attr: string,
    file: FileList
  ): Promise<Res<File>> {
    const createFile: Promise<Res<File>> = new Promise((resolve, reject) => {
      const form: FormData = new FormData();
      form.append('attr', attr);
      form.append('lastModified', file[0].lastModified.toString());
      form.append('type', file[0].type);
      form.append('name', file[0].name);
      form.append('content', file[0], file[0].name);
      const xhr: XMLHttpRequest = new XMLHttpRequest();

      xhr.responseType = 'json';

      xhr.onload = () => {
        const res: Res<File> = xhr.response;
        if (res.status != 200 || !res.args.count) {
          reject(xhr.response);
          return;
        }

        if (res.args.docs?.[0]) {
          res.args.docs[0].file = {
            name: file[0].name,
            lastModified: file[0].lastModified,
            type: file[0].type,
            size: file[0].size,
          };
        }

        resolve(xhr.response);
      };

      xhr.onerror = () => {
        reject(xhr.response);
      };

      xhr.open('POST', `${this.config.api}/${endpoint}`);
      xhr.setRequestHeader('X-Auth-Bearer', this.cacheSession?._id || '');
      xhr.setRequestHeader('X-Auth-Token', this.cacheSession?.token || '');
      xhr.setRequestHeader('X-Auth-App', this.config.appId);
      xhr.send(form);
    });

    return createFile;
  }
}
