import { NawahDI } from './di';
import { AUTH_STATE, CONN_STATE } from './enums';
import {
  Doc,
  File,
  ICallArgs,
  IConfig,
  INawah,
  NawahQuery,
  Res,
  Session,
} from './interfaces';

export class NawahBase implements INawah {
  #config!: IConfig;
  get config(): IConfig {
    return this.#config;
  }

  #queue: {
    auth: Array<{
      endpoint: string;
      callArgs: ICallArgs;
      reject: (res: Res<Doc>) => void;
    }>;
    noAuth: Array<{
      endpoint: string;
      callArgs: ICallArgs;
      reject: (res: Res<Doc>) => void;
    }>;
  } = { auth: [], noAuth: [] };

  #listeners: Record<
    'message' | 'connStateChange' | 'authStateChange',
    Array<{
      condition: (_: Res<unknown> | CONN_STATE | AUTH_STATE) => boolean;
      callable: (_: Res<unknown> | CONN_STATE | AUTH_STATE) => boolean;
      once: boolean;
      tag?: string;
    }>
  > = {
    message: [],
    connStateChange: [],
    authStateChange: [],
  };

  connState: CONN_STATE = CONN_STATE.NOT_CONNECTED;
  authState: AUTH_STATE = AUTH_STATE.NOT_AUTHED;

  session: Session | undefined = undefined;
  #cacheSession!: Session | undefined;
  get cacheSession(): Session | undefined {
    if (!this.#cacheSession) {
      const cacheSessionStr = NawahDI.get('cache').get(this, 'session');
      if (cacheSessionStr) {
        this.#cacheSession = JSON.parse(cacheSessionStr);
      } else {
        this.#cacheSession = this.session;
      }
    }

    return this.#cacheSession;
  }
  set cacheSession(session: Session | undefined) {
    this.#cacheSession = session;

    if (!session) {
      NawahDI.get('cache').remove(this, 'session');
      return;
    }

    NawahDI.get('cache').set(this, 'session', JSON.stringify(session));
  }

  onMessage(res: Res<Doc>): void {
    // Loop over listeners
    for (const [i, listener] of Object.entries(this.#listeners.message)) {
      if (listener.tag) {
        NawahDI.get('logger').log(
          this,
          this.#config,
          `Attempting to run onMessage Event listener with tag: ${listener.tag}`
        );
      }
      // Run filter, callable in try..catch to prevent errors arising from them breaking the loop
      try {
        // Check filter condition
        if (listener?.condition(res)) {
          // Call listener callable
          listener.callable(res);
          // If once, delete listener
          if (listener.once) {
            delete this.#listeners.message[parseInt(i)];
          }
        }
      } catch (err) {
        NawahDI.get('logger').error(
          this,
          this.#config,
          `An error has occurred in running message Event listener: ${err}`
        );
      }
    }
  }

  onConnStateChange(connState: CONN_STATE): void {
    // Prevent duplicate notifications
    if (this.connState == connState) return;
    // Update connState value
    this.connState = connState;
    // If new authState is, attempt to clear auth queue
    if (connState == CONN_STATE.CONNECTED) {
      for (const call of this.#queue.noAuth) {
        this.sendCall(call.endpoint, call.callArgs, call.reject);
      }
    }
    // Loop over listeners
    for (const [i, listener] of Object.entries(
      this.#listeners.connStateChange
    )) {
      // Run filter, callable in try..catch to prevent errors arising from them breaking the loop
      try {
        // Check filter condition
        if (listener?.condition(connState)) {
          // Call listener callable
          listener.callable(connState);
          // If once, delete listener
          if (listener.once) {
            delete this.#listeners.connStateChange[parseInt(i)];
          }
        }
      } catch (err) {
        NawahDI.get('logger').log(
          this,
          this.#config,
          `An error has occurred in running connStateChange Event listener: ${err}`
        );
      }
    }
  }

  onAuthStateChange(authState: AUTH_STATE): void {
    // Prevent duplicate notifications
    if (this.authState == authState) return;
    // Update authState value
    this.authState = authState;
    // If new authState is, attempt to clear auth queue
    if (authState == AUTH_STATE.AUTHED) {
      for (const call of this.#queue.auth) {
        this.sendCall(call.endpoint, call.callArgs, call.reject);
      }
    }
    // Loop over listeners
    for (const [i, listener] of Object.entries(
      this.#listeners.authStateChange
    )) {
      // Run filter, callable in try..catch to prevent errors arising from them breaking the loop
      try {
        // Check filter condition
        if (listener?.condition(authState)) {
          // Call listener callable
          listener.callable(authState);
          // If once, delete listener
          if (listener.once) {
            delete this.#listeners.authStateChange[parseInt(i)];
          }
        }
      } catch (err) {
        NawahDI.get('logger').log(
          this,
          this.#config,
          `An error has occurred in running authStateChange Event listener: ${err}`
        );
      }
    }
  }

  connect(config: IConfig): void {
    // Set config
    this.#config = config;

    // Notify listeners of CONNECTED
    this.onConnStateChange(CONN_STATE.CONNECTED);

    // Set session.anonToken
    this.session = {
      _id: 'f00000000000000000000012',
      token: config.anonToken,
    } as Session;

    this.addEventListener('message', {
      condition: (res) => !!res?.args?.session,
      callable: (res) => {
        NawahDI.get('logger').log(
          this,
          this.#config,
          'Response has session obj'
        );
        if (res?.args?.session?._id == 'f00000000000000000000012') {
          if (this.authState == AUTH_STATE.AUTHED) {
            this.session = undefined;
            this.onAuthStateChange(AUTH_STATE.NOT_AUTHED);
          }

          this.cacheSession = undefined;
          NawahDI.get('logger').log(
            this,
            this.#config,
            'log',
            'Session is null'
          );
        } else {
          this.cacheSession = res.args.session;
          this.session = res.args.session;
          this.onAuthStateChange(AUTH_STATE.AUTHED);
          NawahDI.get('logger').log(this, this.#config, 'Session updated');
        }
      },
      tag: 'NAWAH',
    });
  }

  addEventListener<T extends Doc>(
    event: 'message',
    options: {
      condition: (res: Res<T>) => boolean;
      callable: (res: Res<T>) => void;
      once?: boolean;
      tag?: string;
    }
  ): void;
  addEventListener(
    event: 'connStateChange',
    options: {
      condition: (connState: CONN_STATE) => boolean;
      callable: (connState: CONN_STATE) => void;
      once?: boolean;
      tag?: string;
    }
  ): void;
  addEventListener(
    event: 'authStateChange',
    options: {
      condition: (authState: AUTH_STATE) => boolean;
      callable: (authState: AUTH_STATE) => void;
      once?: boolean;
      tag?: string;
    }
  ): void;
  addEventListener<T extends Doc>(
    event: 'message' | 'connStateChange' | 'authStateChange',
    options:
      | {
          condition: (res: Res<T>) => boolean;
          callable: (res: Res<T>) => void;
          once?: boolean;
          tag?: string;
        }
      | {
          condition: (connState: CONN_STATE) => boolean;
          callable: (connState: CONN_STATE) => void;
          once?: boolean;
          tag?: string;
        }
      | {
          condition: (authState: AUTH_STATE) => boolean;
          callable: (authState: AUTH_STATE) => void;
          once?: boolean;
          tag?: string;
        }
  ): void {
    this.#listeners[event].push({
      condition: options.condition as (...values: Array<unknown>) => boolean,
      callable: options.callable as (...values: Array<unknown>) => boolean,
      once: options.once || false,
      tag: options.tag,
    });
  }

  call<T extends Doc>(endpoint: string, callArgs: ICallArgs): Promise<Res<T>> {
    const call = new Promise<Res<T>>((resolve, reject) => {
      callArgs.query = callArgs.query || {};
      callArgs.doc = callArgs.doc || {};
      callArgs.awaitAuth = callArgs.awaitAuth || false;
      callArgs.call_id = Math.random().toString(36).substring(7);

      NawahDI.get('logger').log(this, this.#config, 'callArgs', callArgs);

      this.addEventListener<T>('message', {
        condition: (res) => res.args.call_id == callArgs.call_id,
        callable: (res) => {
          NawahDI.get('logger').log(
            this,
            this.#config,
            'message received from observer on call_id:',
            res,
            callArgs.call_id
          );
          if (res.status == 200) {
            resolve(res);
          } else {
            reject(res);
          }
        },
        once: true,
        tag: callArgs.call_id,
      });

      if (
        (this.connState == CONN_STATE.CONNECTED &&
          callArgs.awaitAuth &&
          this.authState == AUTH_STATE.AUTHED) ||
        (this.connState == CONN_STATE.CONNECTED && !callArgs.awaitAuth) ||
        endpoint == 'conn/verify'
      ) {
        return this.sendCall(
          endpoint,
          callArgs,
          reject,
        );
      }

      NawahDI.get('logger').warn(
        this,
        this.#config,
        'Connection not yet established. Queuing call:',
        callArgs
      );
      if (callArgs.awaitAuth) {
        NawahDI.get('logger').warn(this, this.#config, 'Queuing in auth queue');
        this.#queue.auth.push({
          endpoint,
          callArgs,
          reject,
        });
      } else {
        NawahDI.get('logger').warn(
          this,
          this.#config,
          'Queuing in noAuth queue'
        );
        this.#queue.noAuth.push({
          endpoint,
          callArgs,
          reject,
        });
      }
    });

    return call;
  }

  callCreateFile(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    endpoint: string,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    attr: string,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    file: unknown
  ): Promise<Res<File>> {
    throw Error('Not implemented');
  }

  sendCall(
    endpoint: string,
    callArgs: ICallArgs,
    reject: (res: Res<Doc>) => void,
  ): void {
    NawahDI.get('logger').log(
      this,
      'About to call:',
      endpoint,
      'with calArgs:',
      callArgs
    );
    try {
      const headers: Record<string, string> = {};
      if (this.session?._id != 'f00000000000000000000012') {
        headers['X-Auth-Bearer'] = this.session?._id || '';
      }
      if (this.session?.token != this.#config.anonToken) {
        headers['X-Auth-Token'] = this.session?.token || '';
      }
      if (this.#config.appId) {
        headers['X-Auth-App'] = this.#config.appId;
      }

      if (callArgs.query) {
        if (!callArgs.doc) {
          callArgs.doc = {};
        }
        callArgs.doc['$query'] = callArgs.query;
      }

      NawahDI.get('http')
        .post<Doc>(this, endpoint, headers, callArgs.doc || {})
        .then((res) => {
          NawahDI.get('logger').log(
            this,
            this.#config,
            `Call '${endpoint}' with 'call_id' '${callArgs.call_id}' response code: ${res.status}`
          );
          if (callArgs.call_id) {
            res.args.call_id = callArgs.call_id;
          }
          this.onMessage(res);
        });
    } catch (err) {
      const callStr = JSON.stringify(callArgs);
      NawahDI.get('logger').error(
        this,
        this.#config,
        'Failed to send call:',
        callStr
      );
      NawahDI.get('logger').error(this, this.#config, 'Error:', err);
      reject(err);
    }
  }

  generateAuthHash(authVar: string, authVal: string, password: string): string {
    if (this.#config.authAttrs.indexOf(authVar) == -1 && authVar != 'token') {
      throw new Error(
        `Unknown authVar '${authVar}'. Accepted authAttrs: '${this.#config.authAttrs.join(
          ', '
        )}, token'`
      );
    }
    if (!password.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/)) {
      throw new Error(
        'Password should be 8 chars, contains one lower-case char, one upper-case char, one number at least.'
      );
    }
    return `${authVar}${authVal}${password}${this.#config.anonToken}`;
  }

  auth(
    authVar: string,
    authVal: string,
    password: string,
    groups?: Array<string>
  ): Promise<Res<Doc>> {
    if (this.authState == AUTH_STATE.AUTHED)
      throw new Error('User already authed.');
    if (this.#config.authAttrs.indexOf(authVar) == -1) {
      throw new Error(
        `Unknown authVar '${authVar}'. Accepted authAttrs: '${this.#config.authAttrs.join(
          ', '
        )}'`
      );
    }
    this.authState = AUTH_STATE.AUTHING;
    const doc: { [key: string]: string | Array<string> } = {
      hash: this.generateAuthHash(authVar, authVal, password),
    };
    doc[authVar] = authVal;
    doc.groups = groups || [];
    const call = this.call<Doc>('session/auth', {
      doc: doc,
    }).catch((err) => {
      this.onAuthStateChange(AUTH_STATE.AUTHING);
      NawahDI.get('logger').error(this, this.#config, 'auth call err:', err);
      return err;
    });

    return call;
  }

  reauth(
    sid?: string,
    token?: string,
    groups?: Array<string>
  ): Promise<Res<Doc>> {
    sid ??= this.cacheSession?._id;
    token ??= this.cacheSession?.token;

    if (
      sid == 'f00000000000000000000012' ||
      token == this.#config.anonToken ||
      !sid ||
      !token
    ) {
      throw Error('No credentials cached or provided.');
    }
    this.onAuthStateChange(AUTH_STATE.AUTHING);
    const query: NawahQuery = {
      $pipe: [
        { _id: { $eq: sid || 'f00000000000000000000012' } },
        { token: { $eq: token || this.#config.anonToken } },
        { groups: { $eq: groups || [] } },
      ],
    };
    const call: Promise<Res<Doc>> = this.call<Doc>('session/reauth', {
      query: query,
    }).catch((err) => {
      NawahDI.get('logger').error(this, this.#config, 'reauth call err:', err);
      this.cacheSession = undefined;
      this.session = undefined;
      this.onAuthStateChange(AUTH_STATE.NOT_AUTHED);
      throw err;
    });

    return call;
  }

  signout(): Promise<Res<Doc>> {
    if (this.authState != AUTH_STATE.AUTHED || !this.session)
      throw new Error('User not authed.');
    const call = this.call('session/signout', {
      query: { $pipe: [{ _id: { $eq: this.session._id } }] },
    }).catch((err) => {
      NawahDI.get('logger').error(this, this.#config, 'signout call err:', err);
      return err;
    });

    return call;
  }

  checkAuth(groups?: Array<string>): Promise<Res<Doc>> {
    NawahDI.get('logger').log(this, this.#config, 'attempting checkAuth');

    const call = this.reauth(undefined, undefined, groups);
    return call;
  }
}
