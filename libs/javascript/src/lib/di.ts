import {
  INawahCache,
  INawahHttp,
  INawahLogger,
} from './interfaces';

class _NawahDI {
  #dependencies: {
    http?: INawahHttp;
    logger?: INawahLogger;
    cache?: INawahCache;
  } = {};

  get(key: 'http'): INawahHttp;
  get(key: 'logger'): INawahLogger;
  get(key: 'cache'): INawahCache;
  get(
    key: 'http' | 'logger' | 'cache'
  ): INawahHttp | INawahLogger | INawahCache {
    if (this.#dependencies[key] == undefined) {
      throw Error(`No value for injection key '${key}'`);
    }

    return this.#dependencies[key] as never;
  }

  set(key: 'http', value: INawahHttp): void;
  set(key: 'logger', value: INawahLogger): void;
  set(key: 'cache', value: INawahCache): void;
  set(
    key: 'http' | 'logger' | 'cache',
    value: INawahHttp | INawahLogger | INawahCache
  ): void {
    this.#dependencies[key] = value as never;
  }
}

export const NawahDI = new _NawahDI();
